from .models import User


def hello(user: User):
    """Returns Hello ***

    :param user: Input user
    """
    return f"Hello {user.first_name} {user.last_name}!"
