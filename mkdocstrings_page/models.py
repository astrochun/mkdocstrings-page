from pydantic import BaseModel


class User(BaseModel):
    """User model"""

    id: int
    first_name: str
    last_name: str
    email: str
