# Mkdocstrings Page

Base repo to publish Python source docs on GitLab pages using `mkdocs` and `mkdocstrings`

## Installation

```bash
pip install -e .[dev]
pre-commit install
```
